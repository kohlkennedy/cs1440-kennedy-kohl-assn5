from Gradient import Gradient
from colour import Color


class BlueGradient(Gradient):
    def __init__(self, iterations):
        self.iterations = iterations
        blk = Color('black')
        blu = Color('blue')
        
        self.grad = list(blk.range_to(blu, self.iterations))
    def getColor(self, n):
        

        # Slice off the 0th element so that I don't have two adjacent copies of 'black'
        return self.grad[n].hex_l
