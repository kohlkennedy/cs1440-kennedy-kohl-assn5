#!/bin/env python3
from Fractal import Fractal
from Gradient import Gradient
# Julia Set Visualizer
#This is the function to get a color for a julia
class Julia(Fractal):

        def __init__(self, config):
                self.config = config



        def color(self,inputedComplexNumber):
                #Sets the complex nukber
                complex_number = complex(float(self.config['creal']), float(self.config['cimag']))
                for i in range(int(self.config['iterations'])):
                        #Reassigns the complex number
                        inputedComplexNumber = inputedComplexNumber * inputedComplexNumber + complex_number  # Iteratively compute z1, z2, z3 ...
                        if abs(inputedComplexNumber) > 2:
                                return i  # The sequence is unbounded
                return int(self.config['iterations']) - 1
