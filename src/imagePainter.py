import sys
from Gradient import Gradient
import Julia
import Mandelbrot
from tkinter import Tk, Canvas, PhotoImage, mainloop


class ImagePainter:
    # This initializes the values to be used later
    def __init__(self, fractal, gradient):
        self.gradient = gradient
        self.fractal = fractal

    def display(self):
        height = width = int(self.fractal.config['pixels'])
        # Sets the window to be used as the gui
        window = Tk()
        # Sets the fractal variavle
        self.fractal.config = self.fractal.config
        imagename = self.fractal.config['imagename']
        # Initializes the image to be used
        image = PhotoImage(width=width, height=height)
        # Initializes the min's and maxes for later
        min_x = float(self.fractal.config['centerx']) - (float(self.fractal.config['axislength']) / 2.0)
        max_x = float(self.fractal.config['centerx']) + (float(self.fractal.config['axislength']) / 2.0)
        min_y = float(self.fractal.config['centery']) - (float(self.fractal.config['axislength']) / 2.0)
        # Sets the image size based off of the info of the fractal
        pixelsize = abs(max_x - min_x) / height
        # Displays the image on the screen
        canvas = Canvas(window, width=width, height=height, bg=self.gradient.getColor(0))
        canvas.pack()
        canvas.create_image((height / 2, width / 2),
                            image=image, state="normal")
        # This is used to create the x and y colors
        for row in range(width, 0, -1):
            for column in range(width):
                # Sets the x and y's
                xCoordinateOfImage = min_x + column * pixelsize
                yCoordinateOfImage = min_y + row * pixelsize
                currentColor = self.gradient.getColor(self.fractal.color(
                        complex(xCoordinateOfImage, yCoordinateOfImage)))
                image.put(currentColor, (column, width - row))
            # Updates the image
            window.update()  # display a row of pixels
        # Output the Fractal into a .png image
        print("The " + imagename + ".png was printed")
        image.write(imagename + ".png")
        # Keeps it running until exited
        mainloop()
