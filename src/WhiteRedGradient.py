from Gradient import Gradient
from colour import Color
class WhiteRedGradient:
    def __init__(self, iterations):
        self.iterations = iterations
        self.wht = Color('white')
        self.red = Color('red')
        self.grad = list(self.wht.range_to(self.red, self.iterations))
    def getColor(self, n):
        return self.grad[n].hex_l