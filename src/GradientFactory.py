from WhiteRedGradient import WhiteRedGradient
from BlueGradient import BlueGradient


def makeGradient(iterationCount, gradName):
    
    if gradName.lower() == "none":
        print("GradientFactory: Creating default color gradient")
        return WhiteRedGradient(iterationCount)
    elif gradName.lower() == "whitered":
        return WhiteRedGradient(iterationCount)
    elif gradName.lower() == "blue":
        return BlueGradient(iterationCount)
    else:
        raise NotImplementedError("Invalid gradient requested")
