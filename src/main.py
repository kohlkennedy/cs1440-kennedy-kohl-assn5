import sys
import FractalFactory
import GradientFactory
from imagePainter import ImagePainter


class Main:

    def main(self, argv):
        # Sets the names of the fractals
        configFileName = "None"
        # Not enough input provided
        if len(sys.argv) > 1:
            configFileName = sys.argv[1]
        gradientName = "None"
        if len(sys.argv) > 2:
            gradientName = sys.argv[2]

        # When the FractalFactory's argument is None it returns the default fractal
        fractal = FractalFactory.makeFractal(configFileName)
        # When the GradientFactory's gtype argument is None it returns the default gradient
        gradient = GradientFactory.makeGradient(int(fractal.config['iterations']), gradientName)

        image = ImagePainter(fractal, gradient)
        image.display()


main = Main()
main.main(sys.argv)
