from Julia import Julia
from Mandelbrot import Mandelbrot
from MotherFractal import MotherFractal

def makeFractal(cfgFile):
    config = checkConfig(cfgFile)
    if cfgFile.lower() == "none":
        print("FractalFactory: Creating default fractal")
        return MotherFractal(config)
    elif config['type'] == 'mandelbrotthree':
        return MotherFractal(config)
    elif config['type'] == "julia":
        return Julia(config)
    elif config['type'] == "mandelbrot":
        return Mandelbrot(config)
    raise NotImplementedError("Invalid fractal requested")

def createConfig(fracName):
    if(fracName.lower() == "none" ):
        fracName = "data\\mandelthree.frac"
    lines = open(fracName)
    config = {}
    for line in lines:
        if line[0] == '#' or line[0] == '\n':
            pass
        else:
            key = line.strip().split(': ')
            config[key[0].lower()] = key[1]
    lines.close()
    return config

def checkConfig(cfgFile):
    config = createConfig(cfgFile)
    if not checkInt(config['centerx']) and not checkFloat(config['centerx']):
        raise NotImplementedError("Invalid fractal requested")
    elif not 'centery' in config:
        raise NotImplementedError("Invalid fractal requested")
    elif not checkInt(config['pixels']):
        raise NotImplementedError("Invalid fractal requested")
    elif not checkInt(config['iterations']):
        raise NotImplementedError("Invalid fractal requested")
    return config

def checkInt(number):
    try:
        int(number)
        return True
    except ValueError:
        return False

def checkFloat(number):
    try:
        float(number)
        return True
    except ValueError:
        return False