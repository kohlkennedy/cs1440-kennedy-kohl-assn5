from Fractal import Fractal


class MotherFractal(Fractal):

    def __init__(self, config):
        self.config = config

    def color(self,inputedComplexNumber):
        #Sets the mandelbrot complex color
                mandelBrotComplexNumber = complex(0, 0)  # z0
                #Sets the max number to the length of the gradients
                MAX_ITERATIONS = int(self.config['iterations'])
                #Goes through the entire gradient list
                for i in range(MAX_ITERATIONS):
                        mandelBrotComplexNumber = mandelBrotComplexNumber * mandelBrotComplexNumber * mandelBrotComplexNumber + inputedComplexNumber  # Get z1, z2, ...
                        if abs(mandelBrotComplexNumber) > 2:
                                return i  # The sequence is unbounded
                return MAX_ITERATIONS - 1   # Indicate a bounded sequence
