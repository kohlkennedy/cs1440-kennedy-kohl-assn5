Documentation for The Fractal Project

Curently, to start the program, the command line must be used. To access this, press the windows button on your keyboard. 

This will bring up the search bar. From here, type in cmd.exe and press enter. After this, type in python. If it doesn't respond with Python followed by the version of python, then download and install that from https://www.python.org/downloads/.

After that is done, type cmd again. From here it will start you in your Users folder. Download the project and leave it in your downloads folder. 

After placing the folder in downloads, type "cd Downloads" without the quotations into the terminal. This will change the directory to downloads. Now, notice the name of the folder the project is, and type cd "project_folder_name".

Now, here is where the fun begins. This program runs three different algorithims to make the fractual. Luckily, we don't need to know which type they are, just their names.

We have a few options to begin. To start the program, you type python main.py. This will create the default fractal, mandelthree and the default color, which is WhiteRed. The next option you have is the type python main.py data/fractal_name.frac . For example, you would type python main.py data/mandelfull.frac and this will run the fullmandel program and use the default color, which is WhiteRed.  The last option, is to type main.py data/fractal_name.frac color_type. Now, there are two options for this one, the blue and the whitered. It isn't case sensitive, which means that you can type it uppercase, lowercase or anyway and it'll function. Now, for the fractals, there are a lot more options.

For the fractals, at least as of this writining, there are twenty four different fractal names. Each of them produces a different pattern, based off of the three different types. The twenty four are: branches.frac, branches256.frac, burningship.frac, connected.frac, elephant.frac, elephants.frac, fulljulia.frac, fullmandelbrot.frac, funnel-down.fraq,hourglass.frac, lakes.frac, leaf.frac, lubber.frac, mandelfour.frac, mandelfull.frac, mandelthree.frac, minibrot.frac, seahorse.frac, spiral-jetty.frac, spiral0.frac, spiral1.frac, unconnected.frac, wholly-squid.frac and zoomed.frac. As you can tell, there are a lot of options.

Now, after all these choices, you decide on your favorite. The program will commence to start drawing it promptly. After the image has been drawn, the program will save said image in the project folder under its name so that you will be able to enjoy this beautiful fractual at your leasure.